/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import TestCarouselTinder from './src/Components/TestCarouselTinder';

AppRegistry.registerComponent(appName, () => TestCarouselTinder);

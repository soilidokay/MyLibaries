import React, {Component, FunctionComponent} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Animated,
  Dimensions,
  PanResponder,
  Easing,
  ActivityIndicator,
  ImageBackground,
} from 'react-native';
import PropTypes from 'prop-types';
import GradItem from './GradItem';
import {
  AnimatedBottom,
  AnimatedLeftBottom,
  AnimatedActive,
} from './ConfigAnimatedTinder';
const {width: widthScreen, height: heightScreen} = Dimensions.get('window');
const HalfWidthScreen = Math.floor(widthScreen / 2);
const HalfHeightScreen = Math.floor(heightScreen / 2);
const LeftRotateValue = -3 * HalfWidthScreen;
const RightRotateValue = 3 * HalfWidthScreen;
const UpRotateValue = -2 * HalfHeightScreen;
const DownRotateValue = 2 * HalfHeightScreen;

const MaxStackValueAnimated = 2 * Math.pow(HalfWidthScreen, 2);
const ComponentLoading = props => {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <ActivityIndicator size="large" color="orange" />
    </View>
  );
};
export const StateVectorEnum = {
  Left: 0,
  Right: 1,
  Up: 2,
  Down: 3,
  None: -1,
};
const GetAnimatedStack = ShadowDirect => {
  switch (ShadowDirect) {
    case 'Bottom':
      return AnimatedBottom;
    default:
      return AnimatedLeftBottom;
  }
};
const ItemCarousel = props => {
  return (
    <View style={StyleSheet.absoluteFill}>
      <GradItem
        AnimatedConfig={props.AnimatedConfig}
        ValueAnimated={props.ValueAnimated}
        index={props.index}
        indexActive={props.indexActive}
        {...{
          WidthContainer: props.widthItem,
          HeightContainer: props.heightItem,
          RotateMin: -30,
          RotateMax: 30,
        }}>
        {props.children}
      </GradItem>
    </View>
  );
};

const data = [
  ...(function*(length) {
    for (let index = 0; index < length; index++) {
      yield index;
    }
  })(10),
];
const DataAddition = [
  ...(function*(length) {
    for (let index = 0; index < length; index++) {
      yield index + 101;
    }
  })(100),
];
const Imgs = [
  require('../img/g1.jpg'),
  require('../img/g2.jpg'),
  require('../img/g3.jpg'),
  require('../img/g4.jpg'),
  require('../img/g5.jpg'),
];
const renderItem = ({item, index}) => {
  return (
    <View
      style={{
        flex: 1,
        margin: 3,
        borderColor: 'lime',
        borderWidth: 1,
        borderRadius: 10,
        padding: 3,
        backgroundColor: '#ccff90',
      }}>
      <ImageBackground
        style={{
          flex: 1,
        }}
        resizeMode={'stretch'}
        source={Imgs[index % 5]}>
        <Text style={{color: 'yellow', margin: 5}}>{item}</Text>
      </ImageBackground>
    </View>
  );
};
const keyExtractor = (item, index) => {
  return item + '';
};

export class CarouselTinder extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true,
    };
    this.ValueAnimated = new Animated.ValueXY(0);
    this.ValueAnimatedStack = this.getAnimatedStack(this.ValueAnimated);

    this.panResponder = PanResponder.create({
      onMoveShouldSetPanResponder: this.onMoveShouldSetPanResponder,
      onPanResponderMove: Animated.event(
        [null, {dx: this.ValueAnimated.x, dy: this.ValueAnimated.y}],
        {
          listener: this.onPanResponderMove,
        },
      ),
      onPanResponderRelease: this.onPanResponderRelease,
      // onPanResponderRelease:(event,gestureState)=>{

      // }
    });
    this.snapToDataBuffer(props.data);
    this.generateData();
  }

  static propTypes = {
    data: PropTypes.array,
    renderItem: PropTypes.func,
    keyExtractor: PropTypes.func,
    amountDisplay: PropTypes.number,
    HeightContainer: PropTypes.number,
    WidthContainer: PropTypes.number,
    onLeft: PropTypes.func,
    onRight: PropTypes.func,
    onUp: PropTypes.func,
    onDown: PropTypes.func,
    thresholdSwipe: PropTypes.number,
    FetchData: PropTypes.func,
    minAmountFetchData: PropTypes.number,
    ComponentLoading: PropTypes.element,
    ShadowDirect: PropTypes.oneOf(['LeftBottom', 'Bottom']),
    DisableRemove: PropTypes.shape({
      Left: PropTypes.bool.isRequired,
      Right: PropTypes.bool.isRequired,
      Up: PropTypes.bool.isRequired,
      Down: PropTypes.bool.isRequired,
    }),
    isRemoveSource: PropTypes.bool,
  };
  static defaultProps = {
    data: data,
    renderItem: renderItem,
    keyExtractor: keyExtractor,
    amountDisplay: 20,
    HeightContainer: heightScreen * 0.8,
    WidthContainer: widthScreen * 0.9,
    onLeft: () => {},
    onRight: () => {},
    onUp: () => {},
    onDown: () => {},

    FetchData: () => {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          resolve(DataAddition.splice(0, 20));
        }, 5000);
      });
      //return []; //DataAddition.splice(0, 3);
    },
    thresholdSwipe: 0.35,
    minAmountFetchData: 21,
    ComponentLoading: <ComponentLoading />,
    ShadowDirect: 'Bottom',
    DisableRemove: {
      Left: false,
      Right: false,
      Up: false,
      Down: false,
    },
    isRemoveSource: true,
  };
  dataQueue = [];
  dataBuffer = [];
  snapToDataBuffer = data => {
    if (this.props.isRemoveSource) {
      this.dataBuffer = data;
    } else {
      this.dataBuffer = [...data];
    }
  };
  getAnimatedStack = ValueAnimated => {
    const {x, y} = ValueAnimated;
    const TempValue = Animated.add(
      Animated.multiply(x, x),
      Animated.multiply(y, y),
    );
    return TempValue.interpolate({
      inputRange: [-1, 0, MaxStackValueAnimated, MaxStackValueAnimated + 1],
      outputRange: [0, 0, 1, 1],
    });
  };
  onChangeDataFromProp = data => {
    this.snapToDataBuffer(data);
    if (this.isLoadingView()) {
      this.generateData();
    }
  };
  ShallowValidate = (Props, nextProps) => {
    if (Props.data !== nextProps.data) {
      this.onChangeDataFromProp(nextProps.data);
    }
    for (let key in nextProps) {
      if (Props[key] != nextProps[key]) return true;
    }
    return false;
  };
  shouldComponentUpdate(nextProps, nextState) {
    return (
      this.ShallowValidate(this.props, nextProps) ||
      this.state.isLoading !== nextState.isLoading
    );
  }
  onMoveShouldSetPanResponder = (event, gestureState) => {
    return Math.abs(gestureState.dx) > 2 || Math.abs(gestureState.dy) > 2;
    // return true;
  };
  onPanResponderMove = (event, gestureState) => {};

  BackItemActiveAnimated = toValue => {
    return new Promise((resolve, reject) => {
      Animated.spring(this.ValueAnimated, {
        toValue,
        useNativeDriver: true,
      }).start(() => resolve(false));
    });
  };
  RemoveItemActiveAnimated = toValue => {
    return new Promise((resolve, reject) => {
      Animated.timing(this.ValueAnimated, {
        toValue,
        duration: 250,
        easing: Easing.bezier(1, 0.32, 0.98, 0.16),
        useNativeDriver: true,
      }).start(() => resolve(true));
    });
  };
  Refresh = () => {
    return new Promise((resolve, reject) => {
      this.setState({isLoading: !this.state.isLoading}, resolve);
    });
  };
  deleteItems = () => {
    if (this.dataQueue.length < 1) return;
    this.dataQueue.splice(
      this.indexActive + 1,
      this.dataQueue.length - this.indexActive - 1,
    );
  };
  getStateVector = gestureState => {
    let vector = Math.abs(gestureState.dx) < Math.abs(gestureState.dy);
    if (vector) {
      let threshold = Math.abs(gestureState.dy) / heightScreen;
      if (threshold < this.props.thresholdSwipe) return StateVectorEnum.None;
      if (gestureState.dy < 0) {
        return StateVectorEnum.Up;
      } else {
        return StateVectorEnum.Down;
      }
    } else {
      let threshold = Math.abs(gestureState.dx) / widthScreen;
      if (threshold < this.props.thresholdSwipe) return StateVectorEnum.None;
      if (gestureState.dx < 0) {
        return StateVectorEnum.Left;
      } else {
        return StateVectorEnum.Right;
      }
    }
  };

  _onLeft = async () => {
    return await this.RemoveItemActiveAnimated({x: LeftRotateValue, y: 0});
  };
  _onRight = async () => {
    return await this.RemoveItemActiveAnimated({x: RightRotateValue, y: 0});
  };
  _onUp = async () => {
    return await this.RemoveItemActiveAnimated({x: 0, y: UpRotateValue});
  };
  _onDown = async () => {
    return await this.RemoveItemActiveAnimated({x: 0, y: DownRotateValue});
  };

  getDataActive = () => {
    return this.dataQueue[this.indexActive];
  };
  NextCard = async vector => {
    let isRemove = await this.ActionNextCard(vector);
    if (isRemove) {
      this.ReloadCard();
    }
  };
  ActionNextCard = async vector => {
    let data = this.getDataActive();
    const {DisableRemove} = this.props;
    let PromiseEvent = null;
    switch (vector) {
      case StateVectorEnum.Up:
        if (!DisableRemove.Up) {
          PromiseEvent = this._onUp();
          this.props.onUp(data);
        }
        break;
      case StateVectorEnum.Down:
        if (!DisableRemove.Down) {
          PromiseEvent = this._onDown();
          this.props.onDown(data);
        }
        break;
      case StateVectorEnum.Left:
        if (!DisableRemove.Left) {
          PromiseEvent = this._onLeft();
          this.props.onLeft(data);
        }
        break;
      case StateVectorEnum.Right:
        if (!DisableRemove.Right) {
          PromiseEvent = this._onRight();
          this.props.onRight(data);
        }
        break;
    }
    if (PromiseEvent) return await PromiseEvent;
    return await this.BackItemActiveAnimated(0);
  };
  setupEventStateVector = async gestureState => {
    let Temp = this.getStateVector(gestureState);
    let isRemove = await this.ActionNextCard(Temp);
    return isRemove;
  };
  ReloadCard = () => {
    if (this.indexActive > -1) {
      this.indexActive--;
    }
    if (this.indexActive < 4) {
      this.deleteItems();
      this.generateData();
    }
    this.Refresh();
  };
  onPanResponderRelease = (event, gestureState) => {
    this.setupEventStateVector(gestureState).then(isDelete => {
      if (isDelete) {
        this.ReloadCard();
      }
    });
  };
  isLoadData = false;
  onFetchData = () => {
    if (this.isLoadData) return;
    this.isLoadData = true;
    if (this.dataBuffer.length < this.props.minAmountFetchData) {
      setTimeout(() => {
        let data = this.props.FetchData();
        if (typeof data === 'object' && data.constructor.name === 'Promise') {
          data.then(this.AdditionData).catch(() => this.AdditionData([]));
        } else {
          this.AdditionData(data);
        }
      }, 10);
    }
  };

  generateData = () => {
    let tempData = this.getDataFromProp().reverse();
    this.indexActive += tempData.length;
    this.dataQueue = tempData.concat(this.dataQueue);
    this.onFetchData();
  };
  isLoadingView = () => {
    return this.indexActive < 3;
  };
  AdditionData = data => {
    console.log('AdditionData', data);
    if (Array.isArray(data) && data.length > 1) {
      this.dataBuffer.splice(this.dataBuffer.length - 1, 0, ...data);
      if (this.isLoadingView()) {
        this.generateData();
        this.Refresh();
      }
    }
    this.isLoadData = false;
  };
  getDataFromProp = () => {
    const {amountDisplay} = this.props;
    return this.dataBuffer.splice(0, amountDisplay);
  };

  getConfigItem = () => {
    return {
      widthItem: this.props.WidthContainer,
      heightItem: this.props.HeightContainer,
      ShadowDirect: this.props.ShadowDirect,
    };
  };
  getConfigItemActive = () => {
    return {
      AnimatedConfig: AnimatedActive,
      ValueAnimated: this.ValueAnimated,
    };
  };
  getConfigItemNonActive = AnimatedConfig => {
    return {
      AnimatedConfig,
      ValueAnimated: this.ValueAnimatedStack,
    };
  };
  indexActive = -1;
  generateItem = () => {
    let AnimatedStack = GetAnimatedStack(this.props.ShadowDirect);
    let TempViews = this.dataQueue.map((item, index) => {
      const isActive = this.indexActive == index;
      return (
        <ItemCarousel
          key={this.props.keyExtractor(item, index)}
          index={index}
          indexActive={this.indexActive}
          {...(isActive
            ? this.getConfigItemActive()
            : this.getConfigItemNonActive(AnimatedStack))}
          {...this.getConfigItem()}>
          {this.props.renderItem({item, index})}
        </ItemCarousel>
      );
    });
    const {ComponentLoading} = this.props;
    TempViews.splice(
      0,
      0,
      <View
        key="ComponentLoading"
        style={{
          width: this.props.WidthContainer,
          height: this.props.HeightContainer,
        }}>
        {ComponentLoading}
      </View>,
    );
    return TempViews;
  };
  componentDidUpdate() {
    this.ValueAnimated.setValue({x: 0, y: 0});
  }

  render() {
    return (
      <View
        style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}
        {...this.panResponder.panHandlers}>
        <View
          style={{
            height: this.props.HeightContainer,
            width: this.props.WidthContainer,
          }}>
          {this.generateItem()}
        </View>
      </View>
    );
  }
}

export default CarouselTinder;

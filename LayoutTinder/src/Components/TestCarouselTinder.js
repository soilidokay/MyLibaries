import React, {useState} from 'react';
import {View, Text, SafeAreaView, TouchableOpacity} from 'react-native';
import CarouselTinder, {StateVectorEnum} from './CarouselTinder';
const Button = props => {
  return (
    <TouchableOpacity
      onPress={props.onPress}
      style={{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'lime',
        borderWidth: 1,
        borderColor: 'red',
      }}>
      <Text style={{color: 'white'}}>{props.children}</Text>
    </TouchableOpacity>
  );
};
let refTinder = null;
const TestCarouselTinder = () => {
  const [ShadowDirect, setShadowDirect] = useState('Bottom');
  return (
    <SafeAreaView style={{flex: 1}}>
      <CarouselTinder
        ref={ref => (refTinder = ref)}
        ShadowDirect={ShadowDirect}
      />
      <View style={{height: 30, flexDirection: 'row'}}>
        <Button
          onPress={() => {
            refTinder.NextCard(StateVectorEnum.Left);
          }}>
          Left
        </Button>
        <Button onPress={() => refTinder.NextCard(StateVectorEnum.Right)}>
          Right
        </Button>
        <Button onPress={() => refTinder.NextCard(StateVectorEnum.Up)}>
          Up
        </Button>
        <Button onPress={() => refTinder.NextCard(StateVectorEnum.Down)}>
          Down
        </Button>
      </View>
      <TouchableOpacity
        onPress={() => {
          if (ShadowDirect == 'Bottom') setShadowDirect('LeftBottom');
          else setShadowDirect('Bottom');
        }}
        style={{
          height: 30,
          width: '100%',
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'lime',
        }}>
        <Text style={{color: 'white'}}>Change ShadowDirect</Text>
      </TouchableOpacity>
    </SafeAreaView>
  );
};

export default TestCarouselTinder;

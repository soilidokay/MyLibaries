const AmountDisplayCard = 2;
const ShadowStack = 10;
import {Dimensions} from 'react-native';
const {width: widthScreen} = Dimensions.get('window');
const GetValue = value => {
  return typeof value === 'number' ? value : 0;
};
export const AnimatedActive = props => {
  const {ValueAnimated} = props;
  return {
    transform: [
      {
        translateY: ValueAnimated.y,
      },
      {
        translateX: ValueAnimated.x,
      },
      {
        rotate: ValueAnimated.x.interpolate({
          inputRange: [-props.WidthContainer, 0, props.WidthContainer],
          outputRange: [
            GetValue(props.RotateMin) + 'deg',
            '0deg',
            GetValue(props.RotateMax) + 'deg',
          ],
        }),
      },
    ],
  };
};
export const AnimatedBottom = props => {
  const {ValueAnimated} = props;
  const index = props.indexActive - props.index - 1;
  return index < 0 || index >= AmountDisplayCard
    ? {transform: [{translateX: 2 * widthScreen}], opacity: 0}
    : (() => {
        const RatioScaleNext = (index + 1) * 0.03;
        const RatioScale = index * 0.03;
        const WidthTranslateYNext =
          props.WidthContainer * RatioScaleNext + (index + 1) * ShadowStack;
        const WidthTranslateY = props.WidthContainer * RatioScale + index * 10;

        return {
          transform: [
            {
              translateY: ValueAnimated.interpolate({
                inputRange: [0, 1, 2],
                outputRange: [WidthTranslateYNext, WidthTranslateY, 0],
              }),
            },
            {
              scale: ValueAnimated.interpolate({
                inputRange: [0, 1, 2],
                outputRange: [1 - RatioScaleNext, 1 - RatioScale, 1],
              }),
            },
          ],
          opacity: ValueAnimated.interpolate({
            inputRange: [0, 1, 2],
            outputRange: [
              (AmountDisplayCard - index - 1) / AmountDisplayCard,
              (AmountDisplayCard - index) / AmountDisplayCard,
              1,
            ],
          }),
        };
      })();
};
export const AnimatedLeftBottom = props => {
  const {ValueAnimated} = props;

  const index = props.indexActive - props.index - 1;

  return index < 0 || index >= AmountDisplayCard
    ? {transform: [{translateX: 2 * widthScreen}], opacity: 0}
    : {
        transform: [
          {
            translateY: ValueAnimated.interpolate({
              inputRange: [0, 1, 2],
              outputRange: [(index + 1) * ShadowStack, index * ShadowStack, 0],
            }),
          },
          {
            translateX: ValueAnimated.interpolate({
              inputRange: [0, 1, 2],
              outputRange: [(index + 1) * ShadowStack, index * ShadowStack, 0],
            }),
          },
        ],
        opacity: ValueAnimated.interpolate({
          inputRange: [0, 1, 2],
          outputRange: [
            (AmountDisplayCard - index - 1) / AmountDisplayCard,
            (AmountDisplayCard - index) / AmountDisplayCard,
            1,
          ],
        }),
      };
};

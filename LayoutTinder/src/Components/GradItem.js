import React, {Component} from 'react';
import {Animated} from 'react-native';

const GradItem = (
  props = {
    HeightContainer: 0,
    WidthContainer: 0,
    RotateMin: 0,
    RotateMax: 0,
  },
) => {
  return (
    <Animated.View
      style={[
        {
          height: props.HeightContainer || 200,
          width: props.WidthContainer || 100,
        },
        props.AnimatedConfig(props),
      ]}>
      {props.children}
    </Animated.View>
  );
};


export default GradItem;

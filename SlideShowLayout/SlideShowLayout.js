import React, {PureComponent} from 'react';
import {View, Animated, Platform, Dimensions, FlatList} from 'react-native';
const {height: heightScreen} = Dimensions.get('window');

const {Value, event} = Animated;

let HeightContent = heightScreen * 0.3;
HeightContent = HeightContent < 250 ? 250 : HeightContent;
const IsPlatform = Platform.OS === 'ios';
const AnimatedFlatList = Animated.createAnimatedComponent(FlatList);
export class SlideShowLayout extends PureComponent {
  constructor(props) {
    super(props);
    this.ValueHeightContent = new Value(0);
    this.ActionEventContent = this._EventScrollContent();
    this.IsAnimatedEnd = IsPlatform;
    this.IsTouchEnd = false;
  }
  currentOffsetY = 0;
  isOpen = true;
  _onScroll = ({nativeEvent}) => {
    this.ActionEventContent(nativeEvent);
  };
  _EventScrollContent = () => {
    return event([{contentOffset: {y: this.ValueHeightContent}}]);
  };
  _ActionScrollTo(value) {
    this.refScroll._component.scrollToOffset({offset: value});
    this.isOpen = value === 0;
    this.currentOffsetY = value;
  }

  _onScrollEnd = nativeEvent => {
    if (!this.IsAnimatedEnd || !this.IsTouchEnd) {
      return;
    }

    if (
      nativeEvent.contentOffset.y > HeightContent ||
      nativeEvent.contentOffset.y < 0
    ) {
      return;
    }

    const TempY = nativeEvent.contentOffset.y + 0;
    let temp = TempY - this.currentOffsetY;

    if (Math.abs(temp) > HeightContent * 0.3) {
      if (!this.isOpen) {
        if (temp < 0) {
          this._ActionScrollTo(0);
        }
      } else {
        if (temp > 0) {
          this._ActionScrollTo(HeightContent);
        }
      }
    } else {
      this._ActionScrollTo(this.isOpen ? 0 : HeightContent);
    }
    this.IsAnimatedEnd = this.IsTouchEnd = false;
  };
  componentDidUpdate() {
    this.ValueHeightContent.setOffset(this.isOpen ? 0 : HeightContent);
    this.ValueHeightContent.flattenOffset();
  }
  /**
   * Author:Unmatched TaiNguyen
   */
  getConfigAnimated = () => {
    return {
      transform: [
        {
          translateY: this.ValueHeightContent.interpolate({
            inputRange: [0, HeightContent - 1, HeightContent],
            outputRange: [0, -HeightContent * 0.5, -2 * HeightContent],
          }),
        },
        {
          rotateX: this.ValueHeightContent.interpolate({
            inputRange: [0, HeightContent, HeightContent + 100],
            outputRange: ['0deg', '90deg', '90deg'],
          }),
        },
        {
          scaleY: Animated.divide(
            this.ValueHeightContent.interpolate({
              inputRange: [0, HeightContent],
              outputRange: [HeightContent, 0],
            }),
            HeightContent,
          ),
        },
      ],
    };
  };
  /**
   * Author:Unmatched TaiNguyen
   */
  getConfigFlatList = () => {
    return {
      ref: ref => (this.refScroll = ref),
      extraData: {data: this.props.data},
      showsVerticalScrollIndicator: false,
      style: this.props.style,
      contentContainerStyle: {paddingTop: HeightContent},
      renderItem: ({item}) => {
        return item;
      },
      onScroll: event(
        [{nativeEvent: {contentOffset: {y: this.ValueHeightContent}}}],
        {useNativeDriver: true},
      ),
      onMomentumScrollEnd: ({nativeEvent}) => {
        this.IsAnimatedEnd = true;
        this._onScrollEnd(nativeEvent);
      },
      onScrollEndDrag: ({nativeEvent}) => {
        this.IsTouchEnd = true;
        this._onScrollEnd(nativeEvent);
      },
      keyExtractor: (item, index) => {
        return index + '';
      },
      data: this.props.data,
    };
  };
  render() {
    return (
      <View style={{flex: 1}}>
        <AnimatedFlatList {...this.getConfigFlatList()} />

        {/* <Animated.View
            style={[
              {
                height: this.ValueHeightContent.interpolate({
                  inputRange: [0, HeightContent],
                  outputRange: [HeightContent, 0],
                }),
              },
              this.props.ContentStyle,
            ]}> */}
        <Animated.View
          pointerEvents={'box-none'}
          style={[
            {
              position: 'absolute',
              height: HeightContent,
              left: 0,
              right: 0,
            },
            this.getConfigAnimated(),
          ]}>
          {this.props.Content}
        </Animated.View>
        {/* </Animated.View> */}
      </View>
    );
  }
}

export default SlideShowLayout;
